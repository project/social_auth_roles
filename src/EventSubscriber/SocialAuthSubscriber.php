<?php

namespace Drupal\social_auth_roles\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\social_auth\Event\SocialAuthEvents;
use Drupal\social_auth\Event\UserEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Reacts on Social Auth events.
 *
 * @see https://www.drupal.org/docs/8/modules/social-api/social-api-1x/social-auth-1x/social-auth-eventsubscriber-example-module
 */
class SocialAuthSubscriber implements EventSubscriberInterface {

  /**
   * The settings for the module.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  private ImmutableConfig $settings;

  /**
   * The logger channel for the module.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  private LoggerChannelInterface $logger;

  /**
   * Construct a new subscriber.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_channel_factory
   *   The logger channel factory service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, LoggerChannelFactoryInterface $logger_channel_factory) {
    $this->settings = $config_factory->get('social_auth_roles.settings');
    $this->logger = $logger_channel_factory->get('social_auth_roles');
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[SocialAuthEvents::USER_CREATED] = ['onUserCreated'];

    return $events;
  }

  /**
   * Alters the username.
   *
   * @param \Drupal\social_auth\Event\UserEvent $event
   *   The Social Auth user event object.
   */
  public function onUserCreated(UserEvent $event) {
    /** @var \Drupal\user\UserInterface $user */
    $user = $event->getUser();
    foreach ($this->settings->get('social_auth_roles') as $role) {
      if ($role !== 0) {
        $user->addRole($role);
        $this->logger->notice('Role "@role" added to new user @user', [
          '@role' => $role,
          '@user' => $user->getDisplayName(),
        ]);
      }
    }
  }

}
