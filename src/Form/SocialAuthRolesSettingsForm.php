<?php

namespace Drupal\social_auth_roles\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\Role;
use Drupal\user\RoleInterface;

/**
 * Configure example settings for this site.
 */
class SocialAuthRolesSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'social_auth_roles_login_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'social_auth_roles.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('social_auth_roles.settings');

    $userRoles = Role::loadMultiple();
    unset($userRoles[RoleInterface::ANONYMOUS_ID]);
    unset($userRoles[RoleInterface::AUTHENTICATED_ID]);

    $availableRoles = [];
    foreach ($userRoles as $user_role_id => $user_role_name) {
      $availableRoles[$user_role_id] = $user_role_name->label();
    }

    $form['social_auth_roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('New Social User roles'),
      '#description' => $this->t('Select user roles that will be assigned on User Creation via the Social Auth.'),
      '#default_value' => $config->get('social_auth_roles'),
      '#options' => $availableRoles,
      '#multiple' => TRUE,
      '#weight' => 5,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable('social_auth_roles.settings')
      ->set('social_auth_roles', $form_state->getValue('social_auth_roles'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
