CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Social Auth Roles allows Administrators to use an interface to decide what Roles
to automatically assign new users created with Social Auth.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/social_auth_roles

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/social_auth_roles


REQUIREMENTS
------------

This module requires following modules outside of Drupal core:

 * Social Auth - https://www.drupal.org/project/social_auth


INSTALLATION
------------

 * Install the Social Auth Roles module as you would normally install a
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
   further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Configuration > Roles - Social Auth New User
    3. Select which roles you want assigned new accounts on creation via
       Social Auth.


MAINTAINERS
-----------

 * Ethan Aho (eahonet) - https://www.drupal.org/u/eahonet

Supporting organizations:

 * CommonPlaces Interactive - https://www.drupal.org/commonplaces-interactive
